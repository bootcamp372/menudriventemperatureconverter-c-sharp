﻿using System;

namespace MenuDrivenTemperatureConverter
{
    internal class Program
    {
        static void Main(string[] args)
        {

            double conversionOption;

            do
            {
                Console.WriteLine("Choose an option: ");
                Console.WriteLine();
                Console.WriteLine("1 - Fahrenheit to Celsius ");
                Console.WriteLine();
                Console.WriteLine("2 - Convert Celsius to Fahrenheit ");
                Console.WriteLine();
                Console.WriteLine("3 - QUIT ");
                Console.WriteLine();
                try
                {
                    conversionOption = Convert.ToDouble(Console.ReadLine());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: " + e);
                    return;
                }

                if (conversionOption == 1)
                {
                    Console.WriteLine("Convert Celsius to Fahrenheit chosen");
                    CtoF();
                }
                else if (conversionOption == 2)
                {
                    Console.WriteLine("Convert Fahrenheit to Celsius chosen");
                    FtoC();
                }
                else if (conversionOption != 3)
                {
                    Console.WriteLine("**Error: unrecognized command, please enter 1, 2, or 3");
                    break;
                }
            } while (conversionOption != 3);
        }

        static void CtoF()
        {
            double Celsius = 0;
            double Fahrenheit = 0;

            Console.Write("Enter a temperature in Celsius: ");
            try
            {
                Celsius = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Celsius Temp:" + Celsius);
                Fahrenheit = (Celsius * 9) / 5 + 32;
                Console.WriteLine("Fahrenheit Temp = {0:0.0°F}", Fahrenheit);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                return;
            }
        }

        static void FtoC()
        {
            double Celsius = 0;
            double Fahrenheit = 0;
            Console.Write("Enter a temperature in Fahrenheit: ");
            try
            {
                Fahrenheit = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Fahrenheit Temp:" + Fahrenheit);
                Celsius = (Fahrenheit - 32) * 5 / 9;
                Console.WriteLine("Celsius Temp = {0:0.0°C}", Celsius);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e);
                return;
            }
        }
    }
}